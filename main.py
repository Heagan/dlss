import numpy as np
from PIL import ImageGrab
import cv2

while True:
    printscreen_pil = ImageGrab.grab(bbox=(0, 0, 500 , 500))
    printscreen_np = np.array(printscreen_pil)
    cv2.imshow('window', cv2.cvtColor(printscreen_np, cv2.COLOR_BGR2RGB)
    if cv2.waitKey(25) & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        break


